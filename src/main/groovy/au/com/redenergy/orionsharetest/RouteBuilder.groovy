package au.com.redenergy.orionsharetest

import org.apache.camel.Endpoint
import org.apache.camel.EndpointInject
import org.springframework.stereotype.Component

@Component(value = "routeBuilder")
class RouteBuilder extends org.apache.camel.builder.RouteBuilder {

  @EndpointInject(uri = 'file://{{lumo.market.data.orion.outbox}}?move={{lumo.market.data.orion.outbox.archive}}/$simple{date:now:yyyyMM}/$simple{file:name}&delay={{lumo.market.data.orion.outbox.delayMillis}}&readLock={{lumo.market.data.orion.readLock}}&include={{lumo.market.data.orion.outbox.include}}')
  Endpoint orionOutboundEndpoint

  @Override
  void configure() throws Exception {
    log.info("Starting route config")

    from(orionOutboundEndpoint)
        .autoStartup(true)
        .routeId("source")
        .log("Received file [\${file:name}] doing stuff")
        .to("file:///tmp")
  }
}